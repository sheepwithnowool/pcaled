%{
#include "lea.tab.h"
#include "lea.h"

int line = 1;
int col = 1;
%}
%option noyywrap yylineno
%x COMMENT

identifier         [a-zA-Z_][a-zA-Z0-9_]{1,30}
character          [a-zA-Z]
integer            \-?[0-9]*
special_char       [!()*+,\-/:;<=>[\]\^]

%%

<INITIAL>{
  "/*"           {BEGIN(COMMENT);}
  "//".*\n
  ":="           return TOKEN_AFF;
  "&&"           return TOKEN_AND;
  "array"        return TOKEN_ARRAY;
  "begin"        return TOKEN_BEGIN;
  "character"    return TOKEN_CHARACTER;
  "dispose"      return TOKEN_DISPOSE;
  "do"           return TOKEN_DO;
  ".."           return TOKEN_DOTDOT;
  "else"         return TOKEN_ELSE;
  "end"          return TOKEN_END;
  "false"        return TOKEN_FALSE;
  "function"     return TOKEN_FUNCTION;
  ">="           return TOKEN_GE;
  "if"           return TOKEN_IF;
  "<="           return TOKEN_LE;
  "new"          return TOKEN_NEW;
  "!="           return TOKEN_NE;
  "null"         return TOKEN_NULL;
  "of"           return TOKEN_OF;
  "or"           return TOKEN_OR;
  "print"        return TOKEN_PRINT;
  "println"      return TOKEN_PRINTLN;
  "procedure"    return TOKEN_PROCEDURE;
  "then"         return TOKEN_THEN;
  "type"         return TOKEN_TYPE;
  "boolean"      return TOKEN_BOOLEAN;
  "integer"      return TOKEN_INTEGER;
  "return"       return TOKEN_RETURN;
  "true"         return TOKEN_TRUE;
  "var"          return TOKEN_VAR;
  "while"        return TOKEN_WHILE;
  {integer}      return INTEGER;
  {special_char} return *yytext;
  {identifier}   return IDENTIFIER;
  {character}    return CHARACTER;
  \n             {line++; col=0;}
  .
}

<COMMENT>{
  "*/"           {BEGIN(INITIAL);}
  .
  \n             {line++; col=0;}
}

%%
